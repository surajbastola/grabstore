<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>@yield('title')</title>
  @yield('css-links')
</head>

<body class="theme-blue-grey">
  <!-- Page Loader Animation During Page load -->
  @include('admin.partials.loader')
  <!-- End Page Loader Animation -->

  <!-- Overlay For Sidebars -->
  <div class="overlay"></div>
  <!-- End Overlay For Sidebars -->

  <!-- Search Bar -->
  @include('admin.partials.search-bar')
  <!-- End Search Bar -->

  <!-- Top Bar -->
  @include('admin.partials.top-bar')
  <!-- #Top Bar -->

  <section>
    <!-- Left Sidebar -->
    @include('admin.partials.left-side-bar')
    <!-- End Left Sidebar -->
    <!-- Right Sidebar -->
    @include('admin.partials.right-side-bar')
    <!-- End Right Sidebar -->
  </section>
  <!-- Main Content -->
  @yield('content')
  <!-- End Main Content -->
</section>

@yield('js-links')
</body>
</html>
