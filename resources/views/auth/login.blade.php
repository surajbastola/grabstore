@extends('layouts.app')

@section('content')

<div class="login-box">
   <div class="logo">
       <a href="javascript:void(0);">Admin Login<b> </b></a>
       <small>Please Login to continue...</small>
   </div>
   <div class="card">
       <div class="body">
           <form id="sign_in" method="POST"  action="{{ route('admin.login')}}">
             {{csrf_field()}}
               <div class="msg">Sign in to start your session</div>
               <div class="input-group">
                   <span class="input-group-addon">
                       <i class="material-icons">person</i>
                   </span>
                   <div class="form-line{{ $errors->has('email') ? ' error' : '' }}">
                       <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>

                   </div>
                   @if ($errors->has('email'))
                       <label class="error">{{ $errors->first('email') }}</label>
                   @endif
               </div>
               <div class="input-group">
                   <span class="input-group-addon">
                       <i class="material-icons">lock</i>
                   </span>
                   <div class="form-line{{ $errors->has('password') ? ' error' : '' }}">
                       <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                   </div>
                   @if ($errors->has('password'))
                       <label class="error">{{ $errors->first('password') }}</label>
                   @endif
               </div>
               <div class="row">
                   <div class="col-xs-8 p-t-5">
                       <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="rememberme" class="filled-in chk-col-pink">
                       <label for="rememberme">Remember Me</label>
                   </div>
                   <div class="col-xs-4">
                       <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
                   </div>
               </div>
               <div class="row m-t-15 m-b--20">
                   <div class="col-xs-6 align-right">
                       <a href="#">Forgot Password</a>
                   </div>
               </div>
           </form>
       </div>
   </div>
 </div>
@endsection
