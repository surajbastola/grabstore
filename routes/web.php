<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
  return view('welcome');
});
//Before Login Routes
Route::prefix('admin')->group(function(){
  Route::get('/', function(){
    return redirect()->route('admin.login');
  });
  Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
  Route::post('login', 'Auth\LoginController@login');
  Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');
});

//After Login Routes
Route::group(['prefix'=>'admin', 'middleware' => 'auth'], function(){

  Route::prefix('dashboard')->group(function(){
    Route::get('/', 'Admin\DashboardController@index')->name('admin.dashboard.index');
  });
});
